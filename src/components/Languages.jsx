/* eslint-disable */
import React, { useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import AOS from "aos";
import "aos/dist/aos.css";

const Languages = (props) => {
  useEffect(() => {
    AOS.init();
    AOS.refresh();
  });

  return (
    <>
      <section className="sect sect--languages">
        <Container>
          <Row>
            <Col lg={9} md={9}>
              <h1 className="sect__title">{props.data.title}</h1>
              <h2 className="sect__subtitle">{props.data.subtitle}</h2>
            </Col>
          </Row>
          <div className="languages__list">
            {props.data.list ? props.data.list.map((language, idx) => (
              <a key={`${language.title}-${idx}`} href="/#" className="languages__list-item">
                {language.title}
              </a>
            ))
              : "loading.."}
          </div>
          <div className="contribute contribute--box">
            <span className="contribute__badge">
              {props.data.contribute.badge}
            </span>
            <span className="contribute__text">
              {props.data.contribute.text}
            </span>
            <a className="contribute__link" href="/#">{props.data.contribute.link}</a>
          </div>
        </Container>
      </section>
    </>
  );
};

export default Languages;