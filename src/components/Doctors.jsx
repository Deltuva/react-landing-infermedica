/* eslint-disable */
import React, { useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import AOS from "aos";
import "aos/dist/aos.css";

const Doctors = (props) => {
  useEffect(() => {
    AOS.init();
    AOS.refresh();
  });

  return (
    <>
      <section className="sect sect--doctors">
        <Container>
          <Row>
            <Col lg={12} md={12}>
              <h1 className="sect__title">{props.data.title}</h1>
            </Col>
          </Row>
          <div className="doctors__blocks">
            <Container>
              <Row>
                {props.data.blocks ? props.data.blocks.map((block, idx) => (
                  <Col key={`${block.title}-${idx}`} lg={6} md={6} sm={12} xs={12}>
                    <div className="doctors__block">
                      <Row>
                        <Col lg={3} md={3}>
                          <picture className="doctors__block-image" data-aos="zoom-in" data-aos-duration="1000">
                            <object aria-label="svg-object" data={block.icon} type="image/svg+xml"></object>
                          </picture>
                        </Col>
                        <Col lg={9} lg={9}>
                          <div className="doctors__block-title">
                            {block.title}
                          </div>
                          <div className="doctors__block-paragraph">
                            {block.paragraph}
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                ))
                  : "loading.."}
              </Row>
            </Container>
          </div>
          <div className="companies__blocks">
            <h2 className="companies__blocks-subtitle">
              {props.data.companies.title}
            </h2>
            <div className="companies__logos companies--logos">
              {props.data.companies.logos ? props.data.companies.logos.map((logo, idx) => (
                <img key={`${logo.title}-${idx}`} className="logo" src={logo.img} alt="" />
              ))
                : "loading.."}
            </div>
          </div>
        </Container>
      </section>
    </>
  );
};

export default Doctors;