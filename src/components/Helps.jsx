import React, { useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import AOS from "aos";
import "aos/dist/aos.css";

const Helps = (props) => {
  useEffect(() => {
    AOS.init();
    AOS.refresh();
  });

  return (
    <>
      <section className="sect sect--helps">
        <Container>
          <Row>
            <Col lg={12} md={12}>
              <h1 className="sect__title">{props.data.title}</h1>
            </Col>
          </Row>
          <div className="helps__blocks">
            <Container>
              <Row>
                {props.data.blocks ? props.data.blocks.map((block, idx) => (
                  <Col key={`${block.title}-${idx}`} lg={4} md={6} sm={12} xs={12} data-aos="fade-up" data-aos-duration="1000">
                    <div className="helps__block">
                      <picture className="helps__block-image">
                        <object data-aos="zoom-in" data-aos-duration="1000" aria-label="svg-object" data={block.icon} type="image/svg+xml"></object>
                      </picture>
                      <div className="helps__block-title" data-aos="fade-up" data-aos-duration="500">{block.title}</div>
                      <div className="helps__block-paragraph" data-aos="fade-up" data-aos-duration="700">{block.paragraph}</div>
                      <a href="/" className="helps__block-lnk" data-aos="fade-up" data-aos-duration="1000">Check details</a>
                    </div>
                  </Col>
                ))
                  : "loading.."}
              </Row>
            </Container>
          </div>
        </Container>
      </section>
    </>
  );
};

export default Helps;