import { Navbar, Nav } from 'react-bootstrap';
import Logo from '../assets/logo.svg';

const NavBar = () => {
  return (
    <>
      <Navbar collapseOnSelect expand="lg">
        <Navbar.Brand href="/#">
          <img
            alt=""
            src={ Logo }
            width="158"
            height="32"
            className="d-inline-block align-top"
          />{ ' ' }
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="ml-auto">
            <Nav.Link href="/#">Products</Nav.Link>
            <Nav.Link href="/#">Blog</Nav.Link>
            <Nav.Link href="/#">Careers</Nav.Link>
            <Nav.Link href="/#">Contact</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
};

export default NavBar;