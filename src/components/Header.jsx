/* eslint-disable */
import React, { useEffect } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import NavBar from '../components/NavBar';
import AOS from "aos";
import "aos/dist/aos.css";

const Header = (props) => {
  useEffect(() => {
    AOS.init();
    AOS.refresh();
  });

  return (
    <>
      <header className="main__header">
        <Container>
          <NavBar />
          <Container>
            <div className="top__header">
              <Row>
                <Col lg={6} md={6}>
                  <h1 className="top__header-title" data-aos="fade-up" data-aos-duration="500">{props.data.title}</h1>
                  <p className="top__header-paragraph" data-aos="fade-up" data-aos-duration="700">{props.data.paragraph}</p>
                  <a href="/" className="top__header-lnk" data-aos="fade-up" data-aos-duration="1000">{props.data.link}</a>
                </Col>
                <Col lg={6} md={6} data-aos="fade-left" data-aos-duration="1000">
                  <picture className="top__header-vector">
                    <object className="svg-vector" aria-label="svg-object" data={props.data.vector} type="image/svg+xml"></object>
                  </picture>
                </Col>
              </Row>
            </div>
          </Container>
        </Container>
      </header>
    </>
  );
};

export default Header;