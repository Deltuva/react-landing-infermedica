import React, { useEffect } from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import AOS from "aos";
import "aos/dist/aos.css";

const Tools = (props) => {
  useEffect(() => {
    AOS.init();
    AOS.refresh();
  });

  return (
    <>
      <section className="sect sect--tools">
        <Container>
          <Row>
            <Col lg={8} md={8}>
              <h1 className="sect__title">{props.data.title}</h1>
              <p className="sect__paragraph">{props.data.paragraph}</p>
            </Col>
          </Row>
          <div className="tools__cards">
            <Container>
              <Row className="no-gutter">
                {props.data.cards ? props.data.cards.map((card, idx) => (
                  <Col key={`${card.title}-${idx}`} lg={card.col} md={12}>
                    <Card className={`tools__card tools__card--${card.class}`}>
                      <Card.Img
                        variant="bottom"
                        src={card.img}
                        data-aos="zoom-in"
                        data-aos-duration="1000"
                      />
                      <Card.Body>
                        <Card.Title>
                          {card.title}
                        </Card.Title>
                        <Card.Text>
                          {card.paragraph}
                        </Card.Text>
                      </Card.Body>
                      <Card.Body>
                        {card.links ? card.links.map((link, idx) => (
                          <Card.Link key={`${link.title}-${idx}`} href="/#">
                            {link.title}
                            {link.icon && <FontAwesomeIcon
                              icon={faArrowRight}
                            />}
                          </Card.Link>
                        ))
                          : "loading.."}
                      </Card.Body>
                    </Card>
                  </Col>
                ))
                  : "loading.."}
                {props.data.cardsHorizontal ? props.data.cardsHorizontal.map((card, idx) => (
                  <Col key={`${card.title}-${idx}`} lg={card.col} md={12} sm={12}>
                    <Card className={`tools__card tools__card--${card.class}`}>
                      <Row>
                        <Col className="order-2" lg={7} md={{ span: 7, order: 1 }}>
                          <Card.Body>
                            <Card.Title>
                              {card.title}
                            </Card.Title>
                            <Card.Text>
                              {card.paragraph}
                            </Card.Text>
                          </Card.Body>
                          <Card.Body>
                            {card.links ? card.links.map((link, idx) => (
                              <Card.Link key={`${link.title}-${idx}`} href="/#">
                                {link.title}
                                {link.icon && <FontAwesomeIcon
                                  icon={faArrowRight}
                                />}
                              </Card.Link>
                            ))
                              : "loading.."}
                          </Card.Body>
                        </Col>
                        <Col className="order-1" lg={5} md={5}>
                          <Card.Img
                            className="img-fluid"
                            variant="right"
                            src={card.img}
                            data-aos="zoom-in"
                            data-aos-duration="1000"
                          />
                        </Col>
                      </Row>
                    </Card>
                  </Col>
                ))
                  : "loading.."}
              </Row>
            </Container>
          </div>
        </Container>
      </section>
    </>
  );
};

export default Tools;