/* eslint-disable */
import { Container, Row, Col } from 'react-bootstrap';

const Footer = (props) => {
  return (
    <footer className="footer">
      <Container>
        <div className="footer-nav">
          <Row>
            {props.data.navigation.meniu ? props.data.navigation.meniu.map((navigation, idx) => (
              <Col key={`${navigation.title}-${idx}`} lg={2} md={4} sm={6}>
                <p className="footer-nav__subtitle">
                  {navigation.title}
                </p>

                {navigation.links ? navigation.links.map((link, idx) => (
                  <a key={`${link.title}-${idx}`} className="footer-nav__link" href="/#">
                    {link.title}
                  </a>
                ))
                  : "loading.."}
              </Col>
            ))
              : "loading.."}
            <Col lg={6} md={3} sm={12}>
              <div className="footer-social">
                {props.data.socials ? props.data.socials.map((social, idx) => (
                  <a key={`${social.icon}-${idx}`} className={`footer-social__icon ${social.icon}`} href={`/#${social.icon}`} title={`${social.icon}`}></a>
                ))
                  : "loading.."}
              </div>
            </Col>
          </Row>
        </div>
        <div className="footer__copyright">
          &copy; {props.data.copyright} & <a href={props.data.developer_site}>
            {props.data.developer}
          </a>
        </div>
      </Container>
    </footer>
  );
};

export default Footer;