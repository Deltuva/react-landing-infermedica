import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

const TopRisk = (props) => {
  return (
    <>
      <div className="toprisk">
        <div className="toprisk__text">
          { props.data.notify } 
          <a href="/#">
            { props.data.link }
            <FontAwesomeIcon
              icon={ faArrowRight }
            /></a>
        </div>
      </div>
    </>
  );
};

export default TopRisk;
