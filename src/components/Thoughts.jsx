/* eslint-disable */
import React, { useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import AOS from "aos";
import "aos/dist/aos.css";

const Thoughts = (props) => {
  useEffect(() => {
    AOS.init();
    AOS.refresh();
  });

  return (
    <>
      <section className="sect sect--thoughts">
        <Container>
          <Row>
            <Col lg={12} md={12}>
              <h1 className="sect__title">{props.data.title}</h1>
              <h2 className="sect__subtitle">{props.data.subtitle}</h2>
              <a className="thought__link" href="/#">{props.data.link}</a>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
};

export default Thoughts;