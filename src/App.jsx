/* eslint-disable */
import Header from './components/Header';
import Helps from './components/Helps';
import Tools from './components/Tools';
import Doctors from './components/Doctors';
import Languages from './components/Languages';
import Thoughts from './components/Thoughts';
import Footer from './components/Footer';
import TopRisk from './components/TopRisk';
import JsonData from './data/data.json';

const App = () => {
  const landingPageData = JsonData;

  return (
    <>
      <TopRisk data={landingPageData.Toprisk} />
      <Header data={landingPageData.Header} />
      <main className="main__content">
        <Helps data={landingPageData.Helps} />
        <Tools data={landingPageData.Tools} />
        <Doctors data={landingPageData.Doctors} />
        <Languages data={landingPageData.Languages} />
        <Thoughts data={landingPageData.Thoughts} />
      </main>
      <Footer data={landingPageData.Footer} />
    </>
  );
};

export default App;
